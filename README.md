**Setup**

- Import project as maven project using eclipse.
- Application URL : http://localhost:8080
- DB Used :Embaded H2

**Build:Run  "mvn clean install" at project root directory**

**How to Run:**
	1. Go to target directory and Run "java -jar customermanagement-0.0.1-SNAPSHOT.jar"

API Details:

######Create Customer:
```
	Endpoint: 		http://localhost:8080/customer
	Http Method:	POST
	Description: Creates Customer and returns Customer Data.
	Payload:		
	{
	"firstName":"Gaurang",
	"lastName":"Patel",
	"emailId":"gaurang@abc.com",
	"mobileNumber":"1233445"
	}
	Response:
	{
    "code": 100,
    "message": "Success",
    "data": {
        "customerId": 1,
        "firstName": "Gaurang",
        "lastName": "Patel",
        "emailId": "gaurang@abc.com",
        "mobileNumber": "1233445"
    }
	}
```

######List Customer:
```
	Endpoint: 		localhost:8080/customer?offset=0&pageSize=1
	Http Method:	GET
	Description: Get The paginated List of customer for offset(Page Number),pageSize.
	Payload:		
	Response:
	
	{
    "code": 100,
    "message": "Success",
    "data": {
        "customerList": [
            {
                "customerId": 1,
                "firstName": "Gaurang",
                "lastName": "Patel",
                "emailId": "gaurang@gmail.com",
                "mobileNumber": "7878171848"
            }
        ],
        "totalRecord": 2
    }
	}
```

- Get Sim Associated with customer:
```
	Endpoint: 		http://localhost:8080/{id}/sim
	Http Method:	GET
	Description: Get The List of sims associated with customer.
	Payload:		
	Response:
	
	{
    "code": 100,
    "message": "Success",
    "data": [
        {
            "simId": 1,
            "msisdn": 5453453,
            "imsi": 765758,
            "status": 0
        }
    ]
	}
	}
```
	
######Create SIM
```
	Endpoint: 		http://localhost:8080/SIM
	Http Method:	POST
	Description: Create SIM
	Payload:
		{
		"msisdn":"5453453",
		"imsi":"765758",
		"status":"0"
		}
	Response:
	
	{
    "code": 100,
    "message": "Success",
    "data": {
        "simId": 2,
        "msisdn": 5453453,
        "imsi": 765758,
        "status": 0
    }
	}
```

######Link SimCart
```
	Endpoint: 		http://localhost:8080/customer/1/sim
	Http Method:	POST
	Description: Finds the sim with the specified id if it dosen't finds the sim creates new and assignes to the customer.
	Payload:
		{
		"simId": 1,
		"msisdn":"5453453",
		"imsi":"765758",
		"status":"0"
		}
	Response:
	
	{
    "code": 100,
    "message": "Success",
    "data": [
        {
            "simId": 1,
            "msisdn": 5453453,
            "imsi": 765758,
            "status": 0
        }
    ]
}
```


**Points Taken Care while implementation:**

1. Generic Response for all the APIs,
	Response contains three Attributes.
		code: Returns 100 if API performed operation successfully, Can be use to provide different error codes in case of errors ,so consumer can get idea about error from the code.
		message:Can be used to provide sort description/message of error.
		data:Data to be  returned as API response.
2. Bean Validation for Sample API (i.e Create Customer)
3. Custom Exception Handler
4. JUnits for the API.
