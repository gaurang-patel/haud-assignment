package com.haud.customermanagement.sim.dataobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.haud.customermanagement.customer.dataobject.CustomerDO;

@Entity
@Table(name="TBLMSIM")
public class SIM {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_sim")
	@SequenceGenerator(name="seq_sim",sequenceName="SEQ_TBLMSIM",allocationSize = 1, initialValue= 1 )
	long simId;
	
	@Column(name="MSISDN")
	long msisdn;
	
	@Column(name="IMSI")
	long imsi;
	
	@Column(name="STATUS")
	int status;

	@ManyToOne
	@JoinColumn(name="CUSTOMERID")
	private CustomerDO customerId;
	
	public long getSimId() {
		return simId;
	}

	public void setSimId(long simId) {
		this.simId = simId;
	}

	public long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(long msisdn) {
		this.msisdn = msisdn;
	}

	public long getImsi() {
		return imsi;
	}

	public void setImsi(long imsi) {
		this.imsi = imsi;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public CustomerDO getCustomerId() {
		return customerId;
	}

	public void setCustomerId(CustomerDO customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "SIM [simId=" + simId + ", msisdn=" + msisdn + ", imsi=" + imsi + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (imsi ^ (imsi >>> 32));
		result = prime * result + (int) (msisdn ^ (msisdn >>> 32));
		result = prime * result + (int) (simId ^ (simId >>> 32));
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SIM other = (SIM) obj;
		if (imsi != other.imsi)
			return false;
		if (msisdn != other.msisdn)
			return false;
		if (simId != other.simId)
			return false;
		if (status != other.status)
			return false;
		return true;
	}
	
	
	
}
