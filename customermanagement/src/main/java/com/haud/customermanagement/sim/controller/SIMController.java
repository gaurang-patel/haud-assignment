package com.haud.customermanagement.sim.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haud.customermanagement.common.constant.CommonConstant;
import com.haud.customermanagement.common.valueobject.CommonResponseVO;
import com.haud.customermanagement.customer.controller.CustomerController;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.service.SIMService;
import com.haud.customermanagement.sim.valueobject.SIMVO;

@RestController
@RequestMapping("/SIM")
public class SIMController {

	public static final Logger logger = LoggerFactory.getLogger(CustomerController.class.getName());
	
	@Autowired
	SIMService simService;
	
	@PostMapping
	public CommonResponseVO createSIM(@RequestBody SIMVO sim) {
		logger.info("[IN] SIMController.createSIM request"+ sim);
		SIM simDO=simService.createSIM(sim);
		SIMVO simVO=SIMVO.buildFromEntity(simDO);
		CommonResponseVO responseVo= new CommonResponseVO();
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(simVO);
		logger.info("[OUT] CustomerController.createCustomer"+ responseVo);
		return responseVo;
	}
}
