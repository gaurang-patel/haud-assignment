package com.haud.customermanagement.sim.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.haud.customermanagement.sim.dataobject.SIM;

public interface SIMRepository extends PagingAndSortingRepository<SIM, Long>{

}
