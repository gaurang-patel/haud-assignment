package com.haud.customermanagement.sim.service;

import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.valueobject.SIMVO;

public interface SIMService {
	
	public SIM createSIM(SIMVO sim);
	public SIM linkSimWithCustomer(SIMVO sim,CustomerDO customerDo);
	public SIM getSimById(long simId);
	public SIM updateSIM(SIM sim);

}
