package com.haud.customermanagement.sim.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.repository.SIMRepository;
import com.haud.customermanagement.sim.service.SIMService;
import com.haud.customermanagement.sim.valueobject.SIMVO;

@Service
public class SIMServiceImpl implements SIMService{

	@Autowired
	SIMRepository simRepository;
	@Override
	public SIM createSIM(SIMVO simVo) {
		SIM sim= new SIM();
		sim.setImsi(simVo.getImsi());
		sim.setMsisdn(simVo.getMsisdn());
		sim.setStatus(simVo.getStatus());
		simRepository.save(sim);
		return sim;
	}
	
	@Override
	public SIM updateSIM(SIM sim) {
		return simRepository.save(sim);
	}

	@Override
	public SIM linkSimWithCustomer(SIMVO sim, CustomerDO customerDo) {
		Optional<SIM> simDoOptional=simRepository.findById(sim.getSimId());
		SIM simDo=simDoOptional.get();
		simDo.setCustomerId(customerDo);
		simRepository.save(simDo);
		return simDo;
	}
	
	@Override
	public SIM getSimById(long simId) {
		Optional<SIM> simDoOptional=simRepository.findById(simId);
		SIM simDo=simDoOptional.orElse(null);
		return simDo;
	}
	
}
