package com.haud.customermanagement.sim.exception;

public class SIMAlreadyLinkedException extends RuntimeException{

	public SIMAlreadyLinkedException(long id) {
		super("SIM with Id ="+id+ " already linked with other customer");
	}
}
