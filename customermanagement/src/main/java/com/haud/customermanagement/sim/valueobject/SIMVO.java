package com.haud.customermanagement.sim.valueobject;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.haud.customermanagement.sim.dataobject.SIM;

public class SIMVO {
	long simId;
	long msisdn;
	long imsi;
	int status;
	public long getSimId() {
		return simId;
	}
	public void setSimId(long simId) {
		this.simId = simId;
	}
	public long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(long msisdn) {
		this.msisdn = msisdn;
	}
	public long getImsi() {
		return imsi;
	}
	public void setImsi(long imsi) {
		this.imsi = imsi;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	@Override
	public String toString() {
		return "SimVO [simId=" + simId + ", msisdn=" + msisdn + ", imsi=" + imsi + ", status=" + status + "]";
	}
	
	public static SIMVO buildFromEntity(SIM simDO) {
		SIMVO simVo= new SIMVO();
		simVo.setSimId(simDO.getSimId());
		simVo.setImsi(simDO.getImsi());
		simVo.setMsisdn(simDO.getMsisdn());
		simVo.setStatus(simDO.getStatus());
		return simVo;
	}
	
	public static List<SIMVO> buildListFromEntity(Set<SIM> simList){
		List<SIMVO> result = simList.stream().map(temp -> {
			SIMVO obj = new SIMVO();
            obj.setSimId(temp.getSimId());
            obj.setImsi(temp.getImsi());
            obj.setMsisdn(temp.getMsisdn());
            obj.setStatus(temp.getStatus());
            return obj;
        }).collect(Collectors.toList());
		
		return result;
	}
	
}
