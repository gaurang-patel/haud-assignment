package com.haud.customermanagement.customer.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.customer.exception.CustomerNotFoundException;
import com.haud.customermanagement.customer.repository.CustomerRepository;
import com.haud.customermanagement.customer.service.CustomerService;
import com.haud.customermanagement.customer.valueobject.CustomerVO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.exception.SIMAlreadyLinkedException;
import com.haud.customermanagement.sim.service.SIMService;
import com.haud.customermanagement.sim.valueobject.SIMVO;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	@Autowired 
	SIMService simService;

	@Override
	public Page<CustomerDO> getallCustomer(int offset, int pageSize) {
		
		Pageable pageable = PageRequest.of(offset,pageSize);
		Page<CustomerDO> customerPage = customerRepository.findAll(pageable);
		return customerPage;
		
	}
	
	@Override
	public CustomerDO createCustomer(CustomerVO customerVo) {
		CustomerDO customerDo= new CustomerDO();
		customerDo.setCustomerId(customerVo.getCustomerId());
		customerDo.setFirstName(customerVo.getFirstName());
		customerDo.setLastName(customerVo.getLastName());
		customerDo.setMobileNumber(customerVo.getMobileNumber());
		customerDo.setEmailId(customerVo.getEmailId());
		customerRepository.save(customerDo);
		return customerDo;
	
	}
	@Override
	public Set<SIM> LinkSim(long customerId,SIMVO simVo) {
		
		CustomerDO customer=getCustomerById(customerId);
		Set<SIM> simList= customer.getSims();
		SIM sim=simService.getSimById(simVo.getSimId());
		if(sim==null) {
			sim=simService.createSIM(simVo);
		}
		if(sim.getCustomerId()!=null) {
			throw new SIMAlreadyLinkedException(sim.getSimId());
		}
		sim.setCustomerId(customer);
		simService.updateSIM(sim);
		customer=getCustomerById(customerId);
		
		return customer.getSims();
	}
	
	
	@Override
	public CustomerDO getCustomerById(long customerId) {
		Optional<CustomerDO> customerOptionl = customerRepository.findById(customerId);
		return customerOptionl.orElseThrow(()-> new CustomerNotFoundException(customerId));
	}
}
