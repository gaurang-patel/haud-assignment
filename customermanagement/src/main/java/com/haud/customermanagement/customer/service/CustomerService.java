package com.haud.customermanagement.customer.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.customer.valueobject.CustomerVO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.valueobject.SIMVO;


public interface CustomerService {

	
	public Page<CustomerDO> getallCustomer(int offset,int pageSize);
	public CustomerDO createCustomer(CustomerVO customerVo);
	public Set<SIM> LinkSim(long customerId,SIMVO simVo);
	public CustomerDO getCustomerById(long customerId);
	
}
