/**
 * 
 */
package com.haud.customermanagement.customer.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.haud.customermanagement.customer.dataobject.CustomerDO;

/**
 * @author Gaurang
 *
 */
public interface CustomerRepository extends PagingAndSortingRepository<CustomerDO, Long>{

}
