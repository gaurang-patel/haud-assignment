package com.haud.customermanagement.customer.exception;

public class CustomerNotFoundException extends RuntimeException{
	public CustomerNotFoundException (long id) {
		super("Customer with Id="+id+" Not Found");
	}
}
