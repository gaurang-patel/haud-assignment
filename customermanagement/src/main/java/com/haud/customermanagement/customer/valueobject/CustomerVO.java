package com.haud.customermanagement.customer.valueobject;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.modelmapper.ModelMapper;

import com.haud.customermanagement.customer.dataobject.CustomerDO;

public class CustomerVO {
	
	private long customerId;
	@NotBlank(message = "FirstName is mandatory")
	private String firstName;
	@NotBlank(message = "LastName is mandatory")
	private String lastName;
	@NotBlank(message = "Email is mandatory")
	private String emailId;
	private String mobileNumber;
	
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	@Override
	public String toString() {
		return "CustomerVO [customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", mobileNumber=" + mobileNumber + "]";
	}
	
	public static CustomerVO buildFromEntity(CustomerDO customerDO) {
		ModelMapper modelMapper = new ModelMapper();
		CustomerVO customerVo=modelMapper.map(customerDO,CustomerVO.class);
		return customerVo;
	}
	
	
}
