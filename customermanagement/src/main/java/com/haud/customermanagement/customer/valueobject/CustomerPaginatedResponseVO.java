package com.haud.customermanagement.customer.valueobject;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.haud.customermanagement.customer.dataobject.CustomerDO;

public class CustomerPaginatedResponseVO {

	List<CustomerVO> customerList;
	long totalRecord;
	public List<CustomerVO> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(List<CustomerVO> customerList) {
		this.customerList = customerList;
	}
	public long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}
	
	@Override
	public String toString() {
		return "CustomerPaginatedResponseVO [customerList=" + customerList + ", totalRecord=" + totalRecord + "]";
	}
	
	public static CustomerPaginatedResponseVO buildFromEntities(Page<CustomerDO> customerPage) {
		List<CustomerVO> customerList=buildListFromEntity(customerPage.getContent());
		CustomerPaginatedResponseVO customerPaginatedResponseVO= new CustomerPaginatedResponseVO();
		customerPaginatedResponseVO.setCustomerList(customerList);
		customerPaginatedResponseVO.setTotalRecord(customerPage.getTotalElements());
		return customerPaginatedResponseVO;
	}
	private static List<CustomerVO> buildListFromEntity(List<CustomerDO> customerList){
		List<CustomerVO> result = customerList.stream().map(CustomerVO::buildFromEntity).collect(Collectors.toList());
		
		return result;
	}

}
