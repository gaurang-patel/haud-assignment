package com.haud.customermanagement.customer.dataobject;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.haud.customermanagement.sim.dataobject.SIM;

@Entity
@Table(name="TBLMCUSTOMER")
public class CustomerDO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_customer")
	@SequenceGenerator(name="seq_customer",sequenceName="SEQ_TBLMCUSTOMER",allocationSize = 1, initialValue= 1 )
	private long customerId;
	
	@Column(name="FIRSTNAME")
	private String firstName;
	
	@Column(name="LASTNAME")
	private String lastName;
	
	@Column(name="EMAILID")
	private String emailId;
	
	@Column(name="MOBILENUMBER")
	private String mobileNumber;
	
	@OneToMany(mappedBy="customerId",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	Set<SIM> sims;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Set<SIM> getSims() {
		return sims;
	}

	public void setSims(Set<SIM> sims) {
		this.sims = sims;
	}

	@Override
	public String toString() {
		return "CustomerDO [customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", mobileNumber=" + mobileNumber + ", sims=" + sims + "]";
	}
	
	
	

		
	
}
