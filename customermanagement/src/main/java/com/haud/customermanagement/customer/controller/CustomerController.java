package com.haud.customermanagement.customer.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.haud.customermanagement.common.constant.CommonConstant;
import com.haud.customermanagement.common.valueobject.CommonResponseVO;
import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.customer.service.CustomerService;
import com.haud.customermanagement.customer.valueobject.CustomerPaginatedResponseVO;
import com.haud.customermanagement.customer.valueobject.CustomerVO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.valueobject.SIMVO;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	public static final Logger logger = LoggerFactory.getLogger(CustomerController.class.getName());
	
	@GetMapping
	public CommonResponseVO getAllCustomer(@RequestParam @Min(value = 0, message = "offset value must be greater than or equal to 0") int offset,@RequestParam int pageSize) {
		logger.info("[IN] CustomerController.getAllCustomer");
		CommonResponseVO responseVo= new CommonResponseVO();
		Page<CustomerDO> customerPage = customerService.getallCustomer(offset, pageSize);
		CustomerPaginatedResponseVO customerPaginatedResponseVO = CustomerPaginatedResponseVO.buildFromEntities(customerPage);
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(customerPaginatedResponseVO);
		logger.info("[OUT] CustomerController.getAllCustomer"+responseVo);
		return responseVo;
	}
	
	@PostMapping
	public CommonResponseVO createCustomer(@Valid @RequestBody CustomerVO customer) {
		logger.info("[IN] CustomerController.createCustomer request"+ customer);
		CustomerDO customerDo=customerService.createCustomer(customer);
		CustomerVO customerVO=CustomerVO.buildFromEntity(customerDo);
		CommonResponseVO responseVo= new CommonResponseVO();
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(customerVO);
		logger.info("[OUT] CustomerController.createCustomer"+ responseVo);
		return responseVo;
	}
	
	@PostMapping("/{id}/sim")
	public CommonResponseVO linkSim(@RequestBody SIMVO sim,@PathVariable int id) {
		logger.info("[IN] CustomerController.linkSim request"+ sim);
		
		Set<SIM> simCollection=customerService.LinkSim(id, sim);
		CommonResponseVO responseVo= new CommonResponseVO();
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(SIMVO.buildListFromEntity(simCollection));
		logger.info("[OUT] CustomerController.createCustomer"+ responseVo);
		return responseVo;
	}
	@GetMapping("/{id}/sim")
	public CommonResponseVO getSim(@PathVariable int id) {
		logger.info("[IN] CustomerController.getSim");
		
		CustomerDO customerDo=customerService.getCustomerById(id);
		SIMVO.buildListFromEntity(customerDo.getSims());
		CommonResponseVO responseVo= new CommonResponseVO();
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(SIMVO.buildListFromEntity(customerDo.getSims()));
		logger.info("[OUT] CustomerController.createCustomer"+ responseVo);
		return responseVo;
	}
	
	@GetMapping("/{id}")
	public CommonResponseVO getCustomer(@PathVariable int id) {
		logger.info("[IN] CustomerController.getCustomer");
		CommonResponseVO responseVo= new CommonResponseVO();
		CustomerDO customerDO = customerService.getCustomerById(id);
		responseVo.setCode(CommonConstant.SUCCESS_CODE);
		responseVo.setMessage(CommonConstant.SUCCESS_MSG);
		responseVo.setData(CustomerVO.buildFromEntity(customerDO));
		logger.info("[OUT] CustomerController.getAllCustomer"+responseVo);
		return responseVo;
	}
	
		
}
