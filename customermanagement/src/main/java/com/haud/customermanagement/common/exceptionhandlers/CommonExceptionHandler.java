package com.haud.customermanagement.common.exceptionhandlers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.haud.customermanagement.common.constant.CommonConstant;
import com.haud.customermanagement.common.valueobject.CommonResponseVO;
import com.haud.customermanagement.customer.exception.CustomerNotFoundException;
import com.haud.customermanagement.sim.exception.SIMAlreadyLinkedException;

@ControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CustomerNotFoundException.class)
	 protected ResponseEntity<CommonResponseVO> handleEntityNotFound(
	           CustomerNotFoundException ex) {
	    	CommonResponseVO response= new CommonResponseVO();
	    	response.setCode(CommonConstant.ENTITY_NOT_FOUND_CODE);
	    	response.setMessage(ex.getMessage());
		
	    	return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	   }
	
	@ExceptionHandler(SIMAlreadyLinkedException.class)
	 protected ResponseEntity<CommonResponseVO> handleSIMLinkException(
			 SIMAlreadyLinkedException ex) {
	    	CommonResponseVO response= new CommonResponseVO();
	    	response.setCode(CommonConstant.FAIL_CODE);
	    	response.setMessage(ex.getMessage());
		
	    	return new ResponseEntity<>(response,HttpStatus.OK);
	   }
	
	public ResponseEntity<CommonResponseVO> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    CommonResponseVO response= new CommonResponseVO();
    	response.setCode(CommonConstant.FAIL_CODE);
    	response.setData(errors);
	    return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
	}
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		 Map<String, String> errors = new HashMap<>();
		    ex.getBindingResult().getAllErrors().forEach((error) -> {
		        String fieldName = ((FieldError) error).getField();
		        String errorMessage = error.getDefaultMessage();
		        errors.put(fieldName, errorMessage);
		    });
		    CommonResponseVO response= new CommonResponseVO();
	    	response.setCode(CommonConstant.FAIL_CODE);
	    	response.setData(errors);
		    return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
		
	}
	
}
