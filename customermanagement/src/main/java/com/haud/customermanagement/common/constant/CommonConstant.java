package com.haud.customermanagement.common.constant;

public class CommonConstant {

	public static final int SUCCESS_CODE=100;
	public static final int FAIL_CODE=200;
	public static final int ENTITY_NOT_FOUND_CODE=404;
	public static final String SUCCESS_MSG="Success";
}
