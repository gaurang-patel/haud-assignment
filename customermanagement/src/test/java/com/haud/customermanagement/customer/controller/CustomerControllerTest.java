package com.haud.customermanagement.customer.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haud.customermanagement.AbstractTest;
import com.haud.customermanagement.common.valueobject.CommonResponseVO;
import com.haud.customermanagement.customer.dataobject.CustomerDO;
import com.haud.customermanagement.customer.exception.CustomerNotFoundException;
import com.haud.customermanagement.customer.service.CustomerService;
import com.haud.customermanagement.customer.valueobject.CustomerVO;
import com.haud.customermanagement.sim.dataobject.SIM;
import com.haud.customermanagement.sim.exception.SIMAlreadyLinkedException;
import com.haud.customermanagement.sim.valueobject.SIMVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerControllerTest extends AbstractTest {

	@MockBean
	CustomerService customerService;
	
	protected MockMvc mvc;
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	
	CustomerVO customerRequVo;
	CustomerDO customerDo;
	SIMVO simVO;
	SIM simDo1;
	SIM simDo2;
	long customerID=10;
	Set<SIM> simCollection;
	@Before
	public void setUp() {
	      	mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	      	long customerID=10;
			String firstName="Gaurang";
			String lastName="Patel";
			String emailId= "abc@gmail.com";
			customerDo= new CustomerDO();
			customerDo.setCustomerId(customerID);
			customerDo.setFirstName(firstName);
			customerDo.setLastName(firstName);
			customerDo.setEmailId(emailId);
			
			customerRequVo=new CustomerVO();
			customerRequVo.setFirstName(firstName);
			customerRequVo.setLastName(lastName);
			customerRequVo.setEmailId(emailId);
			
			simVO= new SIMVO();
			simVO.setSimId(1);
			simDo1= new SIM();
			simDo1.setSimId(1);
			simDo2= new SIM();
			simDo2.setSimId(2);
			simCollection= new HashSet<SIM>();
			simCollection.add(simDo1);
			simCollection.add(simDo2);
			
			
	}
	@Test
	public void findByIdCustomerReturns404_ifNotFound() throws Exception {
		String uri = "/customer/10";
		//long customerID=10;
		Mockito.when(customerService.getCustomerById(customerID)).thenThrow(new CustomerNotFoundException(customerID));
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
			      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		   CommonResponseVO response = super.mapFromJson(content, CommonResponseVO.class);
		   assertEquals(404, response.getCode());
	}
	
	@Test
	public void whenCreatingCustomerReturnsCustomerwithId() throws Exception {
		String uri = "/customer/";
		String payload=super.mapToJson(customerRequVo);
		Mockito.when(customerService.createCustomer(Mockito.any(CustomerVO.class))).thenReturn(customerDo);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
			      .accept(MediaType.APPLICATION_JSON_VALUE).content(payload).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		CommonResponseVO response = super.mapFromJson(content, CommonResponseVO.class);
	    CustomerVO responseCustomerVo= super.mapFromValues(response.getData(), CustomerVO.class);
	    assertEquals(100, response.getCode());
	    assertEquals(customerID, responseCustomerVo.getCustomerId());
		   
	}
	@Test
	public void whenMissingParameterCustomerCreationgivesError() throws Exception {
		String uri = "/customer/";
		customerRequVo.setFirstName(null);
		String payload=super.mapToJson(customerRequVo);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
			      .accept(MediaType.APPLICATION_JSON_VALUE).content(payload).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		CommonResponseVO response = super.mapFromJson(content, CommonResponseVO.class);
	    assertEquals(200, response.getCode());
	}
	@Test
	public void whenLinkingSIMWithCustomerReturnsListofSIMForCustomer() throws Exception {
		String uri = "/customer/10/sim";
		String payload=super.mapToJson(simVO);
		
		Mockito.when(customerService.LinkSim(Mockito.anyLong(), Mockito.any(SIMVO.class))).thenReturn(simCollection);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
			      .accept(MediaType.APPLICATION_JSON_VALUE).content(payload).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		CommonResponseVO response = super.mapFromJson(content, CommonResponseVO.class);
		ObjectMapper objmapper= new ObjectMapper();
		List<SIMVO> simList=objmapper.convertValue(response.getData(), new TypeReference<List<SIMVO>>() {});
	    assertEquals(100, response.getCode());
	    assertEquals(2,simList.size());
	    
	}
	
	@Test
	public void whenTryingTOLinkAlreadyLinkedSIMGivesError() throws Exception {
		String uri = "/customer/10/sim";
		String payload=super.mapToJson(simVO);
		
		Mockito.when(customerService.LinkSim(Mockito.anyLong(), Mockito.any(SIMVO.class))).thenThrow(new SIMAlreadyLinkedException(simVO.getSimId()));
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
			      .accept(MediaType.APPLICATION_JSON_VALUE).content(payload).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		CommonResponseVO response = super.mapFromJson(content, CommonResponseVO.class);
		assertEquals(200, response.getCode());
	}
	
	
}
